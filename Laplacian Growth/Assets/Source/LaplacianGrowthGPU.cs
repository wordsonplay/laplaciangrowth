﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WordsOnPlay.Utils;

public class LaplacianGrowthGPU : MonoBehaviour {
    
  	private static string MAIN_KERNEL = "CSMain";
	private static string FALLOFF_TEXTURE = "Falloff";
	private static string PARTICLE_TEXTURE = "Particles";
	private static string FRINGE_TEXTURE = "Fringe";
	private static string FIELD_TEXTURE = "Field";
	private static string TEXTURE = "Texture";
	private static string BUFFER = "FloatBuffer";
	private static string SUM_BUFFER_IN = "SumBufferIn";
	private static string SUM_BUFFER_OUT = "SumBufferOut";
    private static string SIZE = "Size";
	private static string POSITION = "Position";
	private static string OFFSET = "Offset";
	private static string VALUE = "Value";
	private static string MAIN_TEX = "_MainTex";

    public int size = 1024;
    public int seedX;
    public int seedY;
    public Timer growTimer = 0.1f;
    public float power = 2.0f;


	public ComputeShader falloffShader;
	public ComputeShader fringeShader;
	public ComputeShader fieldShader;
	public ComputeShader sumShader;
	public ComputeShader textureToBufferShader;
	public ComputeShader bufferToTextureShader;

    private Texture2D particleTexture;
    private RenderTexture falloffTexture;
    private RenderTexture fringeTexture;
    private RenderTexture fieldTexture;
    private ComputeBuffer sumBufferIn;
    private ComputeBuffer sumBufferOut;


    void Start() {
        if (!SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.RFloat)){
            Debug.LogError("RFloat render texture format not supported");
        }

        particleTexture = CreateParticleTexture(size);
        falloffTexture = CreateRenderTexture(size * 2, Color.black);
        fringeTexture = CreateRenderTexture(size, Color.black);
        fieldTexture = CreateRenderTexture(size, Color.white);

        sumBufferIn = new ComputeBuffer(size * size, sizeof(float), ComputeBufferType.Default);
        sumBufferOut = new ComputeBuffer(size * size, sizeof(float), ComputeBufferType.Default);

        InitFalloffTexture();
        AddParticle(10, 16);
        AddParticle(16, 16);
        ComputeFringe();

        ComputeBuffer sumBuffer = SumTexture(fieldTexture);
        RenderTexture sumTexture = CreateRenderTexture(size, Color.white);

        BufferToTexture(sumBuffer, sumTexture);
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.SetTexture(MAIN_TEX, sumTexture);
    }

    private Texture2D CreateParticleTexture(int size) {
        
        Color[] pixels = new Color[size * size];
        for (int i = 0; i < pixels.Length; i++) {
            pixels[i] = Color.black;
        }
        Texture2D texture = new Texture2D(size, size);
        texture.SetPixels(0, 0, size, size, pixels);
        texture.Apply();
        texture.filterMode = FilterMode.Point;

        return texture;
    }

    private RenderTexture CreateRenderTexture(int size, Color clearColor) {
        RenderTexture texture = new RenderTexture(size, size, 0, RenderTextureFormat.RFloat);
        texture.enableRandomWrite = true;
        texture.autoGenerateMips = false;
        texture.filterMode = FilterMode.Point;
        texture.Create();

        // I can't find any docs that say the new texture is guaranteed to be clear, so
        // clear to black
        ClearTexture(texture, clearColor);
        return texture;
    }

    private static void ClearTexture(RenderTexture texture, Color clearColor) {
        RenderTexture rt = UnityEngine.RenderTexture.active;
        UnityEngine.RenderTexture.active = texture;
        GL.Clear(true, true, clearColor);
        UnityEngine.RenderTexture.active = rt;
    }

    private void InitFalloffTexture()
    {
        int kernel = falloffShader.FindKernel(MAIN_KERNEL);
        int[] size = new int[2] { falloffTexture.width, falloffTexture.height };
        falloffShader.SetInts(SIZE, size);
        falloffShader.SetTexture(kernel, FALLOFF_TEXTURE, falloffTexture);
        falloffShader.Dispatch(kernel, falloffTexture.width / 8, falloffTexture.height / 8, 1);
    }

    private void AddParticle(int x, int y) {
        particleTexture.SetPixel(x, y, Color.white);
        particleTexture.Apply();

        int kernel = fieldShader.FindKernel(MAIN_KERNEL);
        fieldShader.SetInts(SIZE, new int[2] { fieldTexture.width, fieldTexture.height });
        fieldShader.SetInts(POSITION, new int[2] { x, y });
        fieldShader.SetTexture(kernel, FALLOFF_TEXTURE, falloffTexture);
        fieldShader.SetTexture(kernel, FIELD_TEXTURE, fieldTexture);
        fieldShader.Dispatch(kernel, fieldTexture.width / 8, fieldTexture.height / 8, 1);
    }

    private void ComputeFringe() {
        int kernel = fringeShader.FindKernel(MAIN_KERNEL);
        int[] size = new int[2] { fringeTexture.width, fringeTexture.height };
        fringeShader.SetInts(SIZE, size);
        fringeShader.SetTexture(kernel, PARTICLE_TEXTURE, particleTexture);
        fringeShader.SetTexture(kernel, FIELD_TEXTURE, fieldTexture);
        fringeShader.SetTexture(kernel, FRINGE_TEXTURE, fringeTexture);

        fringeShader.Dispatch(kernel, fringeTexture.width / 8, fringeTexture.height / 8, 1);
    }

    private ComputeBuffer SumTexture(Texture texture) {

        TextureToBuffer(texture, sumBufferIn);

        int kernel = sumShader.FindKernel(MAIN_KERNEL);
        int[] size = new int[2] { texture.width, texture.height };
        sumShader.SetInts(SIZE, size);
        int[] offset = new int[2] { 1, 0 };
        sumShader.SetInts(OFFSET, offset);

        sumShader.SetBuffer(kernel, SUM_BUFFER_IN, sumBufferIn);
        sumShader.SetBuffer(kernel, SUM_BUFFER_OUT, sumBufferOut);

        sumShader.Dispatch(kernel, texture.width / 8, texture.height / 8, 1);

        return sumBufferOut;
    }

    private void TextureToBuffer(Texture texture, ComputeBuffer buffer) {
        int kernel = textureToBufferShader.FindKernel(MAIN_KERNEL);
        int[] size = new int[2] { texture.width, texture.height };
        textureToBufferShader.SetInts(SIZE, size);
        textureToBufferShader.SetTexture(kernel, TEXTURE, texture);
        textureToBufferShader.SetBuffer(kernel, BUFFER, buffer);

        textureToBufferShader.Dispatch(kernel, texture.width / 8, texture.height / 8, 1);
    }

   private void BufferToTexture(ComputeBuffer buffer, Texture texture) {
        int kernel = bufferToTextureShader.FindKernel(MAIN_KERNEL);
        int[] size = new int[2] { texture.width, texture.height };
        bufferToTextureShader.SetInts(SIZE, size);
        bufferToTextureShader.SetBuffer(kernel, BUFFER, buffer);
        bufferToTextureShader.SetTexture(kernel, TEXTURE, texture);

        bufferToTextureShader.Dispatch(kernel, texture.width / 8, texture.height / 8, 1);
    }

    private float GetPixel(RenderTexture renderTexture, int x, int y) {
        Texture2D texture = new Texture2D(1,1);
        RenderTexture rt = UnityEngine.RenderTexture.active;
        UnityEngine.RenderTexture.active = renderTexture;
        texture.ReadPixels(new Rect(x, y, 1, 1), 0, 0, false);
        texture.Apply();
        UnityEngine.RenderTexture.active = rt;
        Color pixel = texture.GetPixel(0,0);
        return pixel.r;
    }

    void Update() {
        if (growTimer.Tick()) {
            // add particle
        }
    }

}
