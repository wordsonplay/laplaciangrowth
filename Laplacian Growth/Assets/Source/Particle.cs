﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    public float movePeriod = 1.0f;
    public Vector3 startPoint;
    public Vector3 endPoint;

    public float growPeriod = 5.0f;
    public float maxSize = 0.5f;
    
    private float moveTime = 0;
    private float growTime = 0;


    // Start is called before the first frame update
    void Start() {
        transform.localPosition = startPoint;
        transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update() {
        moveTime = Mathf.Min(movePeriod, moveTime + Time.deltaTime);
        growTime = Mathf.Min(growPeriod, growTime + Time.deltaTime);
        
        transform.localPosition = Vector3.Lerp(startPoint, endPoint, moveTime / movePeriod);
        transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * maxSize, growTime / growPeriod);
    }
}
