﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WordsOnPlay.Utils;
using UnityEngine.Assertions;

public class LaplacianGrowth : MonoBehaviour {

    public int size = 1024;
    public Point seed;
    public Timer growTimer = 0.1f;
    public float power = 2.0f;

    public Rect rectangle;
    public Particle particlePrefab;

    private HashSet<Point> particles;
    private Dictionary<Point, float> fringe;
    private Dictionary<Point, Point> parent;

    [System.Serializable]
    public class Point {
        public int x;
        public int y;

        public Point() {
            this.x = 0;
            this.y = 0;
        }
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public override string ToString() {
            return string.Format("({0}, {1})", x, y);
        }

        public override bool Equals(System.Object obj) {
            //Check for null and compare run-time types.
            if ((obj == null) || ! this.GetType().Equals(obj.GetType())) {
                return false;
            }
            else { 
                Point p = (Point) obj; 
                return (x == p.x) && (y == p.y);
            }   
        }

        public override int GetHashCode() {
            return (x << 2) ^ y;
        }
    }


    void Start() {
        particles = new HashSet<Point>();
        fringe = new Dictionary<Point, float>();
        parent = new Dictionary<Point, Point>();

        fringe[seed] = 1.0f;
        parent[seed] = seed;

        growTimer.Reset();
    }

    private void AddParticle(Point p) {
        // record the point is occupied
        this.particles.Add(p);
        Assert.IsTrue(fringe.Remove(p));

        // add charge to every point in the fringe
        // ToList() is needed to avoid iterator error when we update fringe values inside foreach loop
        foreach (Point q in fringe.Keys.ToList()) {
            fringe[q] += ChargeAt(p, q);
        }

        // add its neighbours to the fringe
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++){
                if (i == 0 && j ==0) continue;

                AddNeighbour(p, p.x+i, p.y+j);
            }
        }

        // create a particle
        Particle particle = Instantiate(particlePrefab);
        particle.transform.parent = transform;
        particle.name = "Particle " + p;
        
        float x0 = (float)parent[p].x / size;
        float y0 = (float)parent[p].y / size;
        particle.startPoint = rectangle.Point(x0,y0);

        float x1 = (float)p.x / size;
        float y1 = (float)p.y / size;
        particle.endPoint = rectangle.Point(x1,y1);

    }

    private static float ChargeAt(Point p, Point q) {
        int dx = p.x - q.x;
        int dy = p.y - q.y;
        Assert.IsTrue(dx != 0 || dy != 0);

        float r = Mathf.Sqrt(dx * dx + dy * dy);
        float w = 1.0f - 0.5f / r;
        return w;
    }

    private void AddNeighbour(Point p0, int x, int y) {
        if (x < 0 || x >= size || y < 0 || y >= size) {
            return;
        }

        Point p = new Point(x,y);

        if (!particles.Contains(p)) {
            if (!fringe.ContainsKey(p)) {
                parent[p] = p0;
                fringe[p] = 0.0f;

                foreach (Point q in particles) {
                    fringe[p] += ChargeAt(p, q); 
                }
            }
        }
    }

    void Update() {
        if (growTimer.Tick()) {
            Point p = SelectPoint();
            AddParticle(p);
        }
    }

    private Point SelectPoint() {
        float min = float.PositiveInfinity;
        float max = 0;

        foreach (Point q in fringe.Keys) {
            float w = fringe[q];
            min = Mathf.Min(min, w);
            max = Mathf.Max(max, w);
        }

        // all points have the same charge, weight them equally
        if (max == min) {
            min = 0;
        }

        float totalWeight = 0;
        foreach (Point q in fringe.Keys) {
            float w = (fringe[q] - min) / (max - min);
            totalWeight +=  Mathf.Pow(w, power);
        }

        float r = Random.value * totalWeight;
        foreach (Point q in fringe.Keys) {
            float w = (fringe[q] - min) / (max - min);
            w = Mathf.Pow(w, power);
            if (r < w) {
                return q;
            }
            r -= w;
        }

        // this should never happen
        throw new System.InvalidOperationException("Value out of range."); 
    }

    public void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        rectangle.DrawGizmo(transform);
    }

}
